/*
 * ##########################################################################
 * #                                                                        #
 * #  Copyright (C) 1999-2019 Antidot                                       #
 * #                                                                        #
 * #  Should you receive a copy of this source code, you must check you     #
 * #  have a proper, written authorization of Antidot to hold it. If you    #
 * #  don't have such an authorization, you must DELETE all source code     #
 * #  files in your possession, and inform Antidot of the fact you obtain   #
 * #  these files. Should you not comply to these terms, you can be         #
 * #  prosecuted in the extent permitted by applicable law.                 #
 * #                                                                        #
 * ##########################################################################
 */


function send_to_my_instance(url, user, password, zfile, source) {
  var boundary   = "Boundary_for_FluidTopics";

  var auth = btoa(user + ":"  + password);
  var formData = new FormData();
  // Choix de l'utilisateur à partir d'un input HTML de type file...
  formData.append("userfile", zfile, zfile.name);
  var headers = new Headers({
        "Authorization": "Basic " + auth
      });

  var endpoint = getEndPoint(url, source);

  fetch(endpoint, {
        method: 'POST',
        headers: headers,
        body: formData
    }).then(
        answer => {alert("Sent")},
        wrongAnswer => {alert("Sending failed")}
    );

    return false;
}

function getEndPoint(url, source){
  if (!(url.substring(url.length - 1) == "/")){
    url += "/";
  }
  return url    + "api/admin/khub/sources/" + source + "/upload"
}
